import requests
import json
import sys
import configparser

import base64
import hashlib
import os
from getpass import getpass

def pretty_print_POST(req):
    """
    At this point it is completely built and ready
    to be fired; it is "prepared".

    However pay attention at the formatting used in 
    this function because it is programmed to be pretty 
    printed and may differ from the actual request.
    """
    print('{}\n{}\r\n{}\r\n\r\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\r\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))

# YONAS
# config = configparser.ConfigParser()
# config.read('config.ini')
# defaultconfig = config['DEFAULT']
# creds = config['USER']

# if len(sys.argv) > 2:
# 	datain = sys.argv[1]
# 	filein = sys.argv[2]
    
# 	data=json.loads(datain)

# 	#input validation can go here

# 	datastr=json.dumps(data)

# 	if('case_name' in data):
# 		files = {
# 		    'data': datastr,
# 		    'file': (filein, open(filein, 'rb'))
# 		}

# 		responsefile = requests.post(defaultconfig['UrlFile'], files=files, data=data, auth=(creds['User'],  creds['Password']),timeout=3600)
# 	else:
# 		print("Incorrect JSON")		
# else:
# 	print("Incorrect params")


if len(sys.argv) > 2:
	filein = sys.argv[2]

filepath=filein
filename=os.path.basename(filepath)

with open(filepath, "rb") as f:
    bytes = f.read()
    readable_hash = hashlib.sha256(bytes).hexdigest()
    hashvalue = readable_hash
    encode_string = base64.b64encode(bytes).decode("utf-8")
    content = str(encode_string)

url = "https://bpm6.locard.motivian.com/locard/rest/externalAgent/submitEvidence"

caseID = input("Please input case ID: ")
username = input("Please input BPM username: ")
password = getpass()

payload = "{\r\n    \"caseId\":\""+caseID+"\"\r\n    \"hash\":\""+hashvalue+"\"\r\n    \"filename\":\""+filename+"\"\r\n    \"content\":\""+content+"\"\r\n}"

headers = {
  'user': username+':'+password,
  'Authorization': 'Basic dW5pX21hbDp1bmlfbWFs',
  'Content-Type': 'application/json'
}

# response = requests.Request("POST", url, headers=headers, data=payload)
# prepared = response.prepare()
# pretty_print_POST(prepared)
print('Uploading '+filename+' hash ('+hashvalue+') to Case ID: '+caseID+' as user: <'+username+'>')
response = requests.request("POST", url, headers=headers, data=payload)
print(response.text)